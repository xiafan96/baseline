import numpy as np
import torch
import torch.nn as nn


class SelfAttention(nn.Module):

    def __init__(self, nhid, attn_units, attn_hops, dropout=0.1):
        super(SelfAttention, self).__init__()
        self.drop = nn.Dropout(dropout)
        self.ws1 = nn.Linear(nhid * 2, attn_units, bias=False)
        self.ws2 = nn.Linear(attn_units, attn_hops, bias=False)
        self.tanh = nn.Tanh()
        self.relu = nn.ReLU()
        self.softmax = nn.Softmax(dim=1)
        #        self.init_weights()
        self.attention_hops = attn_hops

    def forward(self, inp, mask=None):
        size = inp.size()  # [bsz, len, nhid*2]
        compressed_embeddings = inp.contiguous().view(-1, size[2])  # [bsz*len, nhid*2]

        hbar = self.relu(self.ws1(self.drop(compressed_embeddings)))  # [bsz*len, attn_units]
        alphas = self.ws2(hbar).view(size[0], size[1], -1)  # [bsz, len, hop]
        alphas = torch.transpose(alphas, 1, 2).contiguous()  # [bsz, hop, len]
        if mask is not None:
            alphas.masked_fill(~mask.unsqueeze(1).byte(), -np.inf)
        alphas = self.softmax(alphas.view(-1, size[1]))  # [bsz*hop, len]
        alphas = alphas.view(size[0], self.attention_hops, size[1])  # [bsz, hop, len]
        return torch.bmm(alphas, inp), alphas


class ChannelAttention(nn.Module):
    def __init__(self, channels, pool, ratio=16):
        super(ChannelAttention, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)  # 自适应平均池化
        self.max_pool = nn.AdaptiveMaxPool2d(1)

        self.fc1 = nn.Conv2d(channels, channels // 8, 1, bias=False)
        self.relu1 = nn.ReLU()
        self.fc2 = nn.Conv2d(channels // 8, channels, 1, bias=False)

        self.sigmoid = nn.Sigmoid()
        self.pool = pool

    def forward(self, x):
        if self.pool == 'avg':
            out = self.fc2(self.relu1(self.fc1(self.avg_pool(x))))
        elif self.pool == 'max':
            out = self.fc2(self.relu1(self.fc1(self.max_pool(x))))
        elif self.pool == 'var':
            var_out = x.view(x.shape[0], x.shape[1], -1)
            var_out = torch.var(var_out, dim=2, keepdim=True)
            var_out = var_out.view(var_out.shape[0], var_out.shape[1], var_out.shape[2], 1)
            out = self.fc2(self.relu1(self.fc1(var_out)))
        elif self.pool == 'avg&max':
            avg_out = self.fc2(self.relu1(self.fc1(self.avg_pool(x))))
            max_out = self.fc2(self.relu1(self.fc1(self.max_pool(x))))
            out = avg_out + max_out
        elif self.pool == 'var-avg':
            out = torch.var(x, dim=3, keepdim=True)
            out = torch.mean(out, dim=2, keepdim=True)
            out = self.fc2(self.relu1(self.fc1(out)))
        elif self.pool == 'var-max':
            out = torch.var(x, dim=3, keepdim=True)
            out = (torch.max(out, dim=2, keepdim=True))[0]
            out = self.fc2(self.relu1(self.fc1(out)))
        else:
            print('pooling not exist+now is ' + self.pool)
        attn_channel = self.sigmoid(out)
        #         out=attn_channel*x
        out = torch.mul(attn_channel, x)
        #         out=torch.sum(torch.mul(attn_channel,x),dim=1)
        return attn_channel, out


# class ChannelAttention_var(nn.Module):
#     def __init__(self, channels, ratio=16):
#         super(ChannelAttention_var, self).__init__()
#         self.fc1   = nn.Conv2d(channels, channels // 8, 1, bias=False)
#         self.relu1 = nn.ReLU()
#         self.fc2   = nn.Conv2d(channels // 8, channels, 1, bias=False)

#         self.sigmoid = nn.Sigmoid()

#     def forward(self, x):
#         var_out = x.view(x.shape[0],x.shape[1],-1)
#         var_out = torch.var(var_out, dim=2,keepdim=True)
#         var_out = var_out.view(var_out.shape[0],var_out.shape[1],var_out.shape[2],1)
#         var_out = self.fc2(self.relu1(self.fc1(var_out)))
#         attn_channel = self.sigmoid(var_out)
# #         out=attn_channel*x
#         out=torch.mul(attn_channel,x)
# #         out=torch.sum(torch.mul(attn_channel,x),dim=1)
#         return out,attn_channel

class SpatialAttention(nn.Module):
    def __init__(self, kernel_size=7):
        super(SpatialAttention, self).__init__()

        assert kernel_size in (3, 7)  # 'kernel size must be 3 or 7'
        padding = 3 if kernel_size == 7 else 1

        self.conv1 = nn.Conv2d(2, 1, kernel_size, padding=padding, bias=False)
        self.sigmoid = nn.Sigmoid()

    def forward(self, x):
        avg_out = torch.mean(x, dim=1, keepdim=True)
        max_out, _ = torch.max(x, dim=1, keepdim=True)
        attn = torch.cat([avg_out, max_out], dim=1)
        attn = self.conv1(attn)
        attn_spatial = self.sigmoid(attn)
        out = torch.mul(attn_spatial, x)
        return out
