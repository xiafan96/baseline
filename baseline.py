#!/usr/bin/env python
# coding: utf-8

# In[1]:


# import torchtracer
# from torchtracer import Tracer
import os
import sys
import time

import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from torch.utils import data
from visdom import Visdom

from model import *
from my_classes_vary import Dataset

# parser = argparse.ArgumentParser()
# parser.add_argument('--fold', help='fold', type=str, dest='fold', default=2)
# args = parser.parse_args()
# fold = args.fold
# In[2]:
fold = '4'

# 随机种子设置
SEED = 1
torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)
torch.cuda.manual_seed_all(SEED)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
gpu = '1'
os.environ["CUDA_VISIBLE_DEVICES"] = gpu  # 设置当前使用GPU为0号
use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")

# In[3]:


# 参数设置
dropout = 0.0
configs = {'batch_size': 40,
           'shuffle': True,
           'num_workers': 4}
max_epochs = 200
feature_path = 'data_db/'

# 选取模型
description = '特征：log语谱图128维，帧长512，帧移256；不同fold基线系统性能'
text = '基线_' + 'fold:' + fold + '+'
# text='基线-无差分对数谱'
# text='平均&最大全局池化+通道全连接层：channel/8+不加和+融入CNN'
# lstm
n_head = 1
att_units = 128
hidden_size = 128
##########
lr = 0.00002
gamma = 0
best = {'ua': 0.0, 'wa': 0.0, 'F1': 0.0, 'ua_epoch': 0, 'wa_epoch': 0, 'F1_epoch': 0,
        'Train_confusion': np.zeros((4, 4)), 'Test_confusion': np.zeros((4, 4))}
dic = {'gpu': gpu, 'gamma': gamma, 'path': feature_path,
       'fold': fold, 'dropout': dropout, 'SEED': SEED, 'n_head': n_head, 'hidden_size': hidden_size}
TIME = True


# In[4]:


# 去除txt文件中的空行
def deleteNull(data):
    for i in data:
        if i == '':
            data.remove(i)
    return data


# In[5]:


# 数据读取
labels = {}
labels_augmentation = {}
dict_emo = {'a': 0, 'h': 1, 'n': 2, 's': 3}
totalFileName = 'list/Impro_Total.txt'
trainFileName = 'list/Impro_Train_' + fold + '.txt'
testFileName = 'list/Impro_Test_' + fold + '.txt'
# 数据增强特征文件
# trainFile_augmentationName='list/Impro_Train_augmentation'+fold+'.txt'
with open(totalFileName, 'r') as f:
    total = f.read().split('\n')
    total = deleteNull(total)
for item in total:
    labels[item] = dict_emo[item[4]]
with open(trainFileName, 'r') as f:
    trainData = f.read().split('\n')
    trainData = deleteNull(trainData)
with open(testFileName, 'r') as f:
    testData = f.read().split('\n')
    testData = deleteNull(testData)
# #数据增强
# with open(trainFile_augmentationName,'r') as f:
#     augmentationData = f.read().split('\n')
#     augmentationData = deleteNull(augmentationData)
#     for item in augmentationData:
#         labels_augmentation[item]=dict_emo[item[4]]

# trainData=trainData+augmentationData
# # print(len(trainData))
# labels=dict(labels,**labels_augmentation)
# # print(len(labels))

partition = {}
partition['train'] = trainData
partition['test'] = testData

training_set = Dataset(partition['train'], labels, path=feature_path)
training_generator = data.DataLoader(training_set, **configs)
validation_set = Dataset(partition['test'], labels, path=feature_path)
validation_generator = data.DataLoader(validation_set, **configs)

# In[6]:


# #generator出来的数据[40, 384, 301]，[40]，[40]
# inputs,labels,duration=iter(training_generator).next()  #next函数返回迭代器下一个
# print(inputs.shape,labels.shape,duration.shape)
# print(len(training_generator))


# In[7]:


# 设置优化器及损失函数
model = Baseline().cuda()

optim = torch.optim.Adam(model.parameters(), lr=lr)
# scheduler= StepLR(optim, step_size=50, gamma=0.2)
loss_func = nn.CrossEntropyLoss()
# loss_func=nn.CrossEntropyLoss(weight=torch.Tensor([3.3, 1.17, 1, 1.73]).cuda()).cuda()


# In[8]:


# 画图名称
localTimeStrs = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
envName = text + localTimeStrs
# envName='测试随机种子'+'_'+text+localTimeStrs
viz = Visdom(env=envName, port='8201')
viz.text(description, opts={'title': '说明'})
viz.text(" ".join(sys.argv), opts={'title': 'running command'})
viz.text(str(dic), opts={'title': 'settings'})
print(envName)

# In[9]:


print(model)

# In[10]:


if TIME == True:
    t1 = time.time()
# 自适应学习率
for epoch in range(max_epochs):
    Y_pred = torch.LongTensor().cuda()
    Y_true = torch.LongTensor().cuda()

    y_pred = torch.LongTensor().cuda()
    y_true = torch.LongTensor().cuda()

    trainBatchLoss = []
    testBatchLoss = []
    # 训练
    model.train()
    for local_batch, local_labels, local_lengths in training_generator:
        # 转换到GPU
        local_batch, local_labels, local_lengths = local_batch.to(device), local_labels.to(device), local_lengths.to(
            device)
        local_batch = local_batch.view(-1, 1, local_batch.size(1), local_batch.size(2))
        inputs = local_batch
        # 输入模型
        outputs = model(local_batch)
        # 记录训练结果
        _, predict = torch.max(outputs, 1)
        Y_pred = torch.cat([Y_pred, predict], 0)
        Y_true = torch.cat([Y_true, local_labels], 0)
        # 梯度清零并反向传播
        optim.zero_grad()
        loss = loss_func(outputs, local_labels)
        loss.backward()
        # 将更新值添加到net的parmeters上
        optim.step()
        #         scheduler.step()
        trainBatchLoss.append(loss.item())

    if TIME == True:
        t2 = time.time()
        print('training model an epoch takes ' + str(int(t2 - t1)) + 'seconds.')
        t1 = t2

    # 测试
    model.eval()
    with torch.set_grad_enabled(False):
        #         for local_batch, local_labels,local_lengths in training_generator:
        #             local_batch, local_labels,local_lengths = local_batch.to(device), local_labels.to(device),local_lengths.to(device)
        #             local_batch=local_batch.view(-1,1,local_batch.size(1),local_batch.size(2))
        #             outputs=model(local_batch)
        #             _, predict = torch.max(outputs, 1)
        #             Y_pred=torch.cat([Y_pred,predict],0)
        #             Y_true=torch.cat([Y_true,local_labels],0)
        for local_batch, local_labels, local_lengths in validation_generator:
            # Transfer to GPU
            local_batch, local_labels, local_lengths = local_batch.to(device), local_labels.to(
                device), local_lengths.to(device)
            local_batch = local_batch.view(-1, 1, local_batch.size(1), local_batch.size(2))
            outputs = model(local_batch)
            loss = loss_func(outputs, local_labels)
            testBatchLoss.append(loss.item())
            # print('testLoss: ', loss)
            _, predict = torch.max(outputs, 1)
            y_pred = torch.cat([y_pred, predict], 0)
            y_true = torch.cat([y_true, local_labels], 0)

    if TIME == True:
        t2 = time.time()
        print('test model(training and testing set) takes ' + str(int(t2 - t1)) + 'seconds.')
        t1 = t2

    y_pred, y_true = y_pred.cpu(), y_true.cpu()
    Y_pred, Y_true = Y_pred.cpu(), Y_true.cpu()
    Confusion = confusion_matrix(Y_true, Y_pred).astype('float16').T
    confusion = confusion_matrix(y_true, y_pred).astype('float16').T
    Wa = precision_score(Y_true, Y_pred, average='micro')
    Ua = recall_score(Y_true, Y_pred, average='macro')
    wa = precision_score(y_true, y_pred, average='micro')
    ua = recall_score(y_true, y_pred, average='macro')
    F1 = f1_score(Y_true, Y_pred, average='weighted')
    f1 = f1_score(y_true, y_pred, average='weighted')
    trainLoss = np.mean(trainBatchLoss)
    testLoss = np.mean(testBatchLoss)
    Confusion = torch.Tensor(Confusion)
    confusion = torch.Tensor(confusion)
    if wa > best['wa']:
        best['wa'] = wa
        best['wa_epoch'] = epoch
    if ua > best['ua']:
        best['ua'] = ua
        best['ua_epoch'] = epoch
    if f1 > best['F1']:
        best['F1'] = f1
        best['F1_epoch'] = epoch
        best['Train_confusion'] = Confusion
        best['Test_confusion'] = confusion
    #         tracer.store(torchtracer.data.Model(model))
    print(epoch)
    print(Confusion)
    print(confusion)
    viz.line(X=np.column_stack(np.array([epoch, epoch])), Y=np.column_stack(np.array([trainLoss, testLoss])),
             opts={'legend': ['trainLoss', "testLoss"], 'title': 'loss'}, win='loss', update='append')
    viz.line(X=np.column_stack(np.array([epoch, epoch, epoch, epoch, epoch, epoch])),
             Y=np.column_stack(np.array([Wa, Ua, wa, ua, F1, f1])),
             opts={'legend': ['trainWA', 'trianUA', 'testWA', 'testUA', 'trainF1', 'testF1'], 'title': 'EMO precision'},
             win='EMO precision', update='append')

# tracer.log(msg='Epoch #{:03d}\ttrain_loss: {:.4f}\ttest_loss: {:.4f}\ttrainWA: {:.4f}\ttrainUA: {:.4f}\ttestUA: {:.4f}\ttestUA: {:.4f}'.format(epoch, trainLoss, testLoss,Wa,Ua,wa,ua),
#            file='losses')
#     tracer.log(msg=str(epoch)+'\n'+str(Confusion)+'\n'+str(confusion)+'\n',file='confusion')


# In[11]:


best['wa'], best['ua'], best['F1'] = "%.2f%%" % (best['wa'] * 100), "%.2f%%" % (best['ua'] * 100), "%.2f%%" % (
            best['F1'] * 100)
print(best)

# In[12]:


viz.text(str(best), opts={'title': 'best'})
viz.text(str(model), opts={'title': 'model'})
viz.save([envName])
# viz.text(text,opts={'title':'说明'})

# In[13]:


# viz.text('修改损失函数权重',opts={'title':'备注'})


# In[ ]:
