import pandas as pd
import torch
from torch.utils import data


class Dataset(data.Dataset):
    'Characterizes a dataset for PyTorch'

    def __init__(self, list_IDs, labels, path='total/'):
        'Initialization'
        self.labels = labels
        self.list_IDs = list_IDs
        self.path = path

    def __len__(self):
        'Denotes the total number of samples'
        return len(self.list_IDs)

    def __getitem__(self, index):
        'Generates one sample of data'
        # Select sample
        ID = self.list_IDs[index]
        ID_tmp = ID.split('.')[0] + '.h5'

        # Load data and get label
        X = pd.read_hdf(self.path + ID_tmp, 'feature')
        #         X = pd.read_csv(self.path + ID, header=None)
        X = torch.Tensor(X.values)
        #         if X.shape[1] > 301:
        #             result=X[:, :301]
        #             X=result
        y = self.labels[ID]
        z = X.shape[1]

        return X, y, z
