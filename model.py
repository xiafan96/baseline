#!/usr/bin/env python
# coding: utf-8

# In[1]:


from uawa_att import *


# In[2]:


class Baseline(nn.Module):
    def __init__(self, hidden_size=128):
        super(Baseline, self).__init__()
        # 输入[40,1,128,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,65,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,33,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,17,39]
        self.conv4 = nn.Sequential(
            nn.Conv2d(48, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,9,20]
        #         self.lstm_fc=nn.Linear(20,1)
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(64 * 9, self.hidden_size, batch_first=True, bidirectional=True)
        self.dnn = nn.Linear(self.hidden_size * 2, 4)

    def forward(self, x):
        out = self.conv1(x)

        out = self.conv2(out)

        out = self.conv3(out)

        out = self.conv4(out)

        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        out, (hn, cn) = self.lstm(out)
        # 输出[40,20,128*2]
        # 取最后一维lstm输出
        out = out[:, -1, :]
        # 取平均
        #         out = torch.mean(out, dim=1,keepdim=True)
        # dnn加权
        #         out=self.lstm_fc(out.transpose(1,2))
        out = out.contiguous().view(out.size()[0], -1)

        out = self.dnn(out)
        return out


class Selfattention(nn.Module):
    def __init__(self, n_head, attn_units, hidden_size=128, dropout=0.0):
        super(Selfattention, self).__init__()
        # 输入[40,1,128,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,65,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,33,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,17,39]
        self.conv4 = nn.Sequential(
            nn.Conv2d(48, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,9,20]
        self.hidden_size = hidden_size
        self.attn_units = attn_units
        self.n_head = n_head
        # LSTM(特征尺度，隐藏层），输入是[batch,时序，特征]，输出是[batch,len，hidden_size]
        self.lstm = nn.LSTM(64 * 9, self.hidden_size, batch_first=True, bidirectional=True)
        self.SelfAttention = SelfAttention(self.hidden_size, self.attn_units, self.n_head, dropout=0.0)
        self.dnn = nn.Linear(self.hidden_size * 2 * self.n_head, 4)

    def forward(self, x):
        out = self.conv1(x)
        out = self.conv2(out)
        out = self.conv3(out)
        # 把通道和特征维度相乘，LSTM的输入是（batch，时序，特征），之后交换特征和时序位置
        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        out, (hn, cn) = self.lstm(out)
        out, attn = self.SelfAttention(out)
        out = out.view(out.size()[0], -1)
        #         print(out.shape)
        #         out = out[:,-1,:]      #取最后一维lstm输出
        out = self.dnn(out)
        return out


class CRNN_length(nn.Module):
    def __init__(self, n_head, attn_units, hidden_size=128, dropout=0.0):
        super(CRNN_length, self).__init__()
        # 输入[40,1,384,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,193,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,97,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,49,39]
        self.hidden_size = hidden_size
        self.attn_units = attn_units
        self.n_head = n_head
        # LSTM(特征尺度，隐藏层），输入是[batch,时序，特征]，输出是[batch,len，hidden_size*2]
        self.lstm = nn.LSTM(48 * 49, self.hidden_size, batch_first=True, bidirectional=True)
        self.SelfAttention = SelfAttention(self.hidden_size, self.attn_units, self.n_head, dropout=0.0)
        self.dnn = nn.Linear(self.hidden_size * 2, 4)

    def forward(self, x, lengths):
        lengths = lengths.clone().detach()
        out = self.conv1(x)
        lengths = lengths // 2 + 1
        out = self.conv2(out)
        lengths = lengths // 2 + 1
        out = self.conv3(out)
        lengths = lengths // 2 + 1
        mask = (torch.arange(lengths[0]).cuda()[None, :] < lengths[:, None]).float()
        # [40,39]
        #         print(mask)
        #         print(mask.shape)
        # 把通道和特征维度相乘，LSTM的输入是（batch，时序，特征），之后交换特征和时序位置
        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        out = torch.nn.utils.rnn.pack_padded_sequence(out, lengths, batch_first=True)
        #         h0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()       #也可以不随机初始化
        #         c0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()
        out, (hn, cn) = self.lstm(out)
        out, _ = torch.nn.utils.rnn.pad_packed_sequence(out, batch_first=True)
        # [40,39,128*2]
        out, attn = self.SelfAttention(out, mask)
        out = out.view(out.size()[0], -1)
        #         print(out.shape)
        #         out = out[:,-1,:]      #取最后一维lstm输出
        out = self.dnn(out)
        return out


class Channelattention(nn.Module):
    def __init__(self, channel_pool, hidden_size=128):
        super(Channelattention, self).__init__()
        # 输入[40,1,128,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,65,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,33,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,17,39]
        self.conv4 = nn.Sequential(
            nn.Conv2d(48, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,9,20]
        #         self.lstm_fc=nn.Linear(20,1)
        self.channel_pool = channel_pool
        self.ChannelAttention3 = ChannelAttention(64, self.channel_pool)
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(64 * 9, self.hidden_size, batch_first=True, bidirectional=True)
        self.dnn = nn.Linear(self.hidden_size * 2, 4)

    def forward(self, x):
        out = self.conv1(x)

        out = self.conv2(out)

        out = self.conv3(out)

        out = self.conv4(out)

        out_channel = out
        attn_channel, out = self.ChannelAttention3(out)

        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        out, (hn, cn) = self.lstm(out)
        # 输出[40,20,128*2]
        # 取最后一维lstm输出
        out = out[:, -1, :]
        # 取平均
        #         out = torch.mean(out, dim=1,keepdim=True)
        # dnn加权
        #         out=self.lstm_fc(out.transpose(1,2))
        out = out.contiguous().view(out.size()[0], -1)

        out = self.dnn(out)
        return out, out_channel, attn_channel


class CRNN_test(nn.Module):
    def __init__(self, hidden_size=128):
        super(CRNN_test, self).__init__()
        # 输入[40,1,128,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,65,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,33,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,17,39]
        self.conv4 = nn.Sequential(
            nn.Conv2d(48, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,9,20]
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(64 * 9, self.hidden_size, batch_first=True, bidirectional=True)
        self.dnn = nn.Linear(self.hidden_size * 2, 4)

    def forward(self, x):
        out = self.conv1(x)

        out = self.conv2(out)

        out = self.conv3(out)

        out = self.conv4(out)

        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        out, (hn, cn) = self.lstm(out)

        out = out[:, -1, :]  # 取最后一维lstm输出

        out = out.contiguous().view(out.size()[0], -1)

        out = self.dnn(out)
        return out


class CRNN_ConvChannel(nn.Module):
    def __init__(self, n_head, attn_units, channel_pool, hidden_size=128, dropout=0.0):
        super(CRNN_ConvChannel, self).__init__()
        # 输入[40,1,384,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,193,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,97,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,49,38]
        # 自适应变长
        #         self.Adap=nn.AdaptiveAvgPool2d([49,20])
        self.conv4 = nn.Conv2d(48, 48, kernel_size=(1, 1))
        self.conv5 = nn.Conv2d(48, 48, kernel_size=(1, 1))
        # 通道注意力，将不同通道乘相应权重相加

        # pool可选avg/max/var/avg+max……
        self.channel_pool = channel_pool
        self.ChannelAttention3 = ChannelAttention(48, self.channel_pool)
        #         self.SpatialAttention=SpatialAttention(kernel_size=3)
        # 输出[40,48,49,39]
        self.hidden_size = hidden_size
        self.attn_units = attn_units
        self.n_head = n_head
        # LSTM(特征尺度，隐藏层），输入是[batch,时序，特征]，输出是[batch,len，hidden_size]
        #         self.lstm = nn.LSTM(49*1,self.hidden_size,batch_first=True,bidirectional=True)
        self.lstm = nn.LSTM(48 * 49, self.hidden_size, batch_first=True, bidirectional=True)
        #         self.SelfAttention=SelfAttention(self.hidden_size,self.attn_units,self.n_head,dropout=0.0)
        #         self.dnn = nn.Linear(self.hidden_size*2*self.n_head,4)
        self.dnn = nn.Linear(self.hidden_size * 2, 4)

    def forward(self, x):
        out = self.conv1(x)
        #         out1,attn_channel = self.ChannelAttention1(out)
        #         out=out1+out

        out = self.conv2(out)
        #         out1,attn_channel= self.ChannelAttention2(out)
        #         out=out1+out

        out = self.conv3(out)
        out1 = self.conv4(out)
        out2 = self.conv5(out)
        attn_out = torch.matmul(out1.transpose(3, 2), out2)

        attn_channel = self.ChannelAttention3(attn_out)
        out = torch.mul(attn_channel, out) + out
        ##是否进行残差
        #         out=out1+out
        #         print(out.shape)
        #         out=self.SpatialAttention(out)
        #         print(out.shape)
        #         out2 = self.conv4(out)
        #         out = out1 + out2
        # 把通道和特征维度相乘，LSTM的输入是（batch，时序，特征），之后交换特征和时序位置
        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        #         h0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()       #也可以不随机初始化
        #         c0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()
        out, (hn, cn) = self.lstm(out)
        out = out[:, -1, :]  # 取最后一维lstm输出
        #         out, attn=self.SelfAttention(out)
        out = out.contiguous().view(out.size()[0], -1)
        #         print(out.shape)

        out = self.dnn(out)
        return out


class CNN_GRU(nn.Module):
    def __init__(self, n_head, attn_units, hidden_size=128, dropout=0.0):
        super(CNN_GRU, self).__init__()
        # 输入[40,1,384,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,193,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,97,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,49,39]
        self.hidden_size = hidden_size
        self.attn_units = attn_units
        self.n_head = n_head
        # LSTM(特征尺度，隐藏层），输入是[batch,时序，特征]，输出是[batch,len，hidden_size]
        self.gru = nn.GRU(48 * 49, self.hidden_size, batch_first=True, bidirectional=True)
        self.SelfAttention = SelfAttention(self.hidden_size, self.attn_units, self.n_head, dropout=0.0)
        self.dnn = nn.Linear(self.hidden_size * 2 * self.n_head, 4)

    def forward(self, x):
        out = self.conv1(x)
        out = self.conv2(out)
        out = self.conv3(out)
        # 把通道和特征维度相乘，LSTM的输入是（batch，时序，特征），之后交换特征和时序位置
        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        h0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()  # 也可以不随机初始化
        out, hn = self.gru(out, h0)
        out, attn = self.SelfAttention(out)
        out = out.view(out.size()[0], -1)
        #         print(out.shape)
        #         out = out[:,-1,:]      #取最后一维lstm输出
        out = self.dnn(out)
        return out


# In[3]:


class MTL(nn.Module):
    def __init__(self, n_head, attn_units, hidden_size=128, dropout=0.0):
        super(MTL, self).__init__()
        # 输入[40,1,384,301]
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,16,193,151]
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,32,97,76]
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        # 输出[40,48,49,39]
        self.hidden_size = hidden_size
        self.attn_units = attn_units
        # LSTM(特征尺度，隐藏层），输入是[batch,时序，特征]，输出是[batch,len，hidden_size]
        self.lstm = nn.LSTM(48 * 49, self.hidden_size, batch_first=True, bidirectional=True)
        self.SelfAttention = SelfAttention(self.hidden_size, self.attn_units, n_head, dropout=0.0)
        self.dnn_emo = nn.Linear(self.hidden_size * 2, 4)
        self.dnn_MTL = nn.Linear(self.hidden_size * 2, 2)

    def forward(self, x):
        out = self.conv1(x)
        out = self.conv2(out)
        out = self.conv3(out)
        # 把通道和特征维度相乘，LSTM的输入是（batch，时序，特征），之后交换特征和时序位置
        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        h0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()  # 也可以不随机初始化
        c0 = torch.randn(2, out.size()[0], self.hidden_size).cuda()
        out, (hn, cn) = self.lstm(out, (h0, c0))
        out, attn = self.SelfAttention(out)
        out = out.view(out.size()[0], -1)
        #         out = out[:,-1,:]      #取最后一维lstm输出
        out_emo = self.dnn_emo(out)
        out_MTL = self.dnn_MTL(out)
        return out_emo, out_MTL

# In[ ]:
